package simplenews;

import java.util.Map.Entry;

import models.NewsStorage;
import models.Post;
import models.Themes;

public class Main {
	
	public static void main(String Args[]) {
		
		NewsStorage newsStorage = new NewsStorage();
		Post post1 = new Post(1,"title1", "filling", Themes.SPORT);
		Post post2 = new Post(2,"title2", "filling", Themes.HUMOR);
		Post post3 = new Post(3,"title3", "filling", Themes.HUMOR);
		newsStorage.save(post1);
		newsStorage.save(post2);
		newsStorage.save(post3);
		
		for(Entry<Integer, Post> post:newsStorage.filterByTheme(Themes.HUMOR)) {
			System.out.println(post.getValue().getTitle());
		}
		
		
		
	}
}
