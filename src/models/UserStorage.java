package models;

import java.util.HashMap;

public class UserStorage {

	HashMap<Integer, User> users = new HashMap<>();

	public HashMap<Integer, User> getUsers() {
		return users;
	}

	public void setUsers(HashMap<Integer, User> users) {
		this.users = users;
	}

}
