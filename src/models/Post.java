package models;

public class Post {
	private int id;
	private String title;
	private String filling;
	private Themes theme;

	public Post(int id, String title, String filling, Themes theme) {
		this.id = id;
		this.title = title;
		this.filling = filling;
		this.theme = theme;
	}

	public int getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getFilling() {
		return filling;

	}

	public void setTitle(String title) {
		this.title = title;

	}

	public void setFilling(String filling) {
		this.filling = filling;
	}

	public Themes getTheme() {
		return theme;
	}

	public void setTheme(Themes theme) {
		this.theme = theme;
	}

}
