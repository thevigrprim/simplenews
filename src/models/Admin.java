package models;

public class Admin extends Guest {

	private int id;
	private String name;
	private Boolean adminStatus;
	private String password;

	public Admin(int id, String name, String password) {
		this.id = id;
		this.name = name;
		this.password = password;
		adminStatus = true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getAdminStatus() {
		return adminStatus;
	}

	public void setAdminStatus(Boolean adminStatus) {
		this.adminStatus = adminStatus;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
