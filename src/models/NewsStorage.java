package models;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class NewsStorage {

	HashMap<Integer, Post> news = new HashMap<>();

	public void save(Post post) {
		if (news.get(post.getId()) == null) {
			news.put(post.getId(), post);
			System.out.println("Post saved succesfully");
		} else {
			System.out.println("Can`t save post with that id =(");
		}
	}

	public void delete(int id) {
		news.remove(id);
	}

	public void showAllTitles() {
		for (Post post : news.values()) {
			System.out.println(post.getTitle());
		}
	}

	public Post filterById(int id) {
		return news.get(id);
	}

	public List<Entry<Integer, Post>> filterByTitle(String title) {
		return news.entrySet().stream().filter(entry -> entry.getValue().getTitle().contains(title))
				.collect(Collectors.toList());
	}

	public List<Entry<Integer, Post>> filterByTheme(Themes theme) {
		return news.entrySet().stream().filter(entry -> entry.getValue().getTheme() == theme)
				.collect(Collectors.toList());
	}

}
